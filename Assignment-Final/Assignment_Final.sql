-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: sp2018secad
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('rahul','*299AE961C68416D9FD21F20018CADD1E1A684FC9'),('raj','*6C0922A315E0F694197AA97071622310574908AE');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `commenter` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `postid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `postid` (`postid`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`postid`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Secure application','Secure application development','Prudhvi Raj Bhoga','2018-05-02 15:59:42',43),(2,'Secure application','Secure application Development','Prudhvi Raj Bhoga','2018-05-02 18:08:45',36),(3,'prudhvi','hdslghgldsjl','Prudhvi Raj Bhoga','2018-05-02 20:40:33',44),(4,'prudjvi','&lt;script&gt;alert(document.cookie);&lt;/script&gt;','Prudhvi Raj Bhoga','2018-05-02 20:40:48',44);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `newtext` text NOT NULL,
  `published` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `owner` varchar(50) DEFAULT NULL,
  `postsenabled` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `users` (`username`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (36,'manikanta','Gru','2018-05-02 20:15:19','prudhvi',0),(43,'heya','hello hi','2018-05-02 20:15:20','prudhvi',0),(44,'alexander','liked the post','2018-05-01 09:28:01','prudhvi',1),(45,'rajjj','raj updated post','2018-05-01 11:14:51','prudhvi',1),(47,'sgwtwuw','shshjs','2018-05-02 06:54:51','ghar',0),(48,'manikanta','created by Mani Kanta','2018-05-02 20:10:17','prudhvi',1),(49,'Prudhvi Raj Bhoga new post','prudhvi Raj ','2018-05-02 20:10:18','prudhvi',1),(50,'prudhvi','&lt;script&gt;alert(document.cookie);&lt;/script&gt;','2018-05-02 20:22:36','prudhvi',1),(53,'hi','&lt;script&gt;alert(&quot;hey&quot;);&lt;/script&gt;','2018-05-02 20:42:44','prudhvi',1);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regular_users`
--

DROP TABLE IF EXISTS `regular_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regular_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `approved` int(1) NOT NULL,
  `enabled` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regular_users`
--

LOCK TABLES `regular_users` WRITE;
/*!40000 ALTER TABLE `regular_users` DISABLE KEYS */;
INSERT INTO `regular_users` VALUES (1,'prudhvi','prudhvi@gmail.com','*94BDCEBE19083CE2A1F959FD02F964C7AF4CFC29',0,0),(2,'test','prudviboga@gmail.com','*DC69A1D2349058C10E3FF1BE6E7DD874349B9931',0,0),(3,'rahul','prudviboga@gmail.com','*DC69A1D2349058C10E3FF1BE6E7DD874349B9931',0,0),(4,'test1','test1@gmail.com','*DC69A1D2349058C10E3FF1BE6E7DD874349B9931',0,0),(5,'test2','prudviboga@gmail.com','*DC69A1D2349058C10E3FF1BE6E7DD874349B9931',0,0),(6,'test3','test3@gmail.com','*DC69A1D2349058C10E3FF1BE6E7DD874349B9931',0,0);
/*!40000 ALTER TABLE `regular_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `approved` int(1) NOT NULL,
  `enabled` int(1) NOT NULL,
  `email` char(180) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`username`),
  UNIQUE KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('ghar','*79F8276CD2B9BDDC4FCD4E5772C1568BA4E8F26D',1,1,'jjsjsj@gmail.com','7799185778',4),('manikanta','*F310399026F63B60F425F7C40CF72112803EA59C',1,1,'mani@gmail.com','7799185778',5),('prudhvi','*8B15FB65A943BE82CB968B75A5C96389C312D53C',1,1,'prudviboga1new@gmail.com','9391100673',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-02 18:36:37
