<?
ini_set("session.cookie_httponly", True);
ini_set("session.cookie_secure", True);
ini_set("session.cookie_lifetime", 900);
session_start();

if (!isset($_SESSION["admin_logged"] ) or $_SESSION["admin_logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
    header("Refresh:0; url=adminform.php");
    die();
  }

   if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){   //validating browser info

  echo "<script>alert('Session hijacking is detected!');</script>";
  header("Refresh:0; url=adminform.php");
  die();
  }
?>