<?php
  
  ini_set("session.cookie_httponly", True);
  ini_set("session.cookie_secure", True);
  ini_set("session.cookie_lifetime", 900);
//session_set_cookie_params(900, "/", "localhost", True, True); //this can also be used to set the cookie parameters 
//setcookie("test_cookie", "test", 900, '/', 'localhost', true, true);
session_start();
$old_sessionid = session_id();
  require 'mysql.php';
  if (isset($_POST["username"]) and isset($_POST["password"]) ){
    
    if (mysql_checklogin_secure($_POST["username"],$_POST["password"])) {
      session_regenerate_id(); //generating a new session ID
      $new_sessionid = session_id();
     // echo" Old Session Id: $old_sessionid<br/>"; //printing the old session ID
      //echo"New Session Id: $new_sessionid<br/>";  //printing the new session ID
      $_SESSION["user_logged"] = TRUE;
      $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];   //to store browser info
      $_SESSION["username"] = $_POST["username"];
      
  }
    else{
       echo "<script>alert('Invalid username/password');</script>";
       unset($_SESSION["user_logged"]); 
    }
  }
  if (!isset($_SESSION["user_logged"] ) or $_SESSION["user_logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
    header("Refresh:0; url=userform.php");
    die();
  }

   if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){   //validating browser info

  echo "<script>alert('Session hijacking is detected!');</script>";
  header("Refresh:0; url=userform.php");
  die();
  }
?>

