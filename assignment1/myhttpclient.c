/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <libgen.h>    //library to get filename directly
#include <arpa/inet.h> //to use getaddrinfo()

int main (int argc, char *argv[])
{
   	printf("Hello World\nPrudhvi Raj bhoga\nSecure Application development\n");
   	//printf("Prudhvi Raj bhoga\n");
   	//printf("Secure Application development\n")
	//printf("hello\n");
    //printf("servername = %s, port=%s\n",argv[1],argv[2]);

	char *url = argv[1];
	char servername[1000]; //= argv[1];
	int port = 80;//= atoi(argv[2]);
	char path[1000];// = argv[2];
	char ip_address[100];//to get Ip address
	
	if(argc!=2){
	printf("usage: %s <URl>\n",argv[0]);
	exit(0);
	}
	//char temp[1024];
	//strncpy(temp,url,strlen(url));
	//strncpy(temp,url,sizeof(url));
	//printf("temp == %s\n",temp );
	sscanf(url, "http://%[^/]%s",servername,path);
	//printf("server = %s\n, path= %s\n",servername,path );
   
	snprintf(url,sizeof(url)-1,"servername = %s, port=%d,path = %s\n",servername, port, path);	

	int sockfd = socket(AF_INET, SOCK_STREAM,0); 
	if(sockfd<0){
		perror("cannot create socket");
		exit(1);
	}

	// to get ipaddress using getaddrinfo

    struct addrinfo hints, *serverinfo, *p;
    struct sockaddr_in *h;
    int rv;
 
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
    hints.ai_socktype = SOCK_STREAM;
 
    if ( (rv = getaddrinfo( servername , "http" , &hints , &serverinfo)) != 0) 
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }
 
    // loop to connect to the server and get ip address
    for(p = serverinfo; p != NULL; p = p->ai_next) 
    {
        h = (struct sockaddr_in *) p->ai_addr;
        strcpy(ip_address , inet_ntoa( h->sin_addr ) );
    }
     
    //to free the addrinfo  
    freeaddrinfo(serverinfo); 

    //printf("Ip Address = %s\n",ip_address);
	//handle the HTTP response code, if it is 200, write to file
	const char ch= '/';
	//char *ret;
	//ret = strchr(path, ch);
	//printf("file name : %s\n",ret);
	char filename[100];
	char str[20] = "index.html";
	if(strrchr(path, '.')==NULL){
		strncpy(filename,str,sizeof(str));
		//printf("file name : %s\n",filename);
	}
	else{
		//printf("hello1");
		//char *bname;
		//char bname[100]; //basename is a library funtion to get file name from url
		strncpy(filename,basename(path),100);
		//strcpy(filename,strrchr(path,ch)+1);
		//printf("file name : %s\n",filename);
		} 

	struct hostent *server_he;
	if((server_he = gethostbyname(servername)) == NULL){
	perror("cannot resolve the hostname");
	exit(2);
	}
	struct sockaddr_in serveraddr;
	bzero((char *) &serveraddr,sizeof(serveraddr));
	serveraddr.sin_family=AF_INET;
	bcopy((char *) server_he->h_addr,
  	(char *) &serveraddr.sin_addr.s_addr,
     	server_he->h_length);
	serveraddr.sin_port=htons(port);
	
	int connected =connect(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	
	if(connected<0){
	perror("cannot connect to the server");
	exit(3);
	}
	//else
	//printf("connecting to the server %s (%s) to retrieve the document %s\n",servername,ip_address,filename);
	char *msg="this is just a test message from client";
	int byte_sent;//=send(sockfd,msg,strlen(msg),0);
	/*char buffer[1024];
	//printf("enter your message:");
	bzero(buffer,1024);
	//sprintf(buffer, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", servername);*/
	
	//define a buffer size constant to be consistent
	const int BUFFER_SIZE = 1024*1024; // 1 Megabyte
	char buffer[BUFFER_SIZE];
	bzero(buffer, BUFFER_SIZE);
	
	sprintf(buffer, "GET %s HTTP/1.0\r\nHost: %s\r\n\r\n",path,servername);
	byte_sent=send(sockfd,buffer,strlen(buffer),0);
	/*receive the first package up to the size of buffer, this data package
		should contain the HTTP headers, so 1 Mbyte buffer is long enough! */
	int bytes_received = recv(sockfd,buffer, BUFFER_SIZE,0);

	//printf("bytes received :%d\n",bytes_received );
	
	if(bytes_received<0)
	{

	perror("error in reading");
	exit(4);
	}

	//printf("buffer = %s\n", buffer);
	//handle the HTTP response code, if it is 200, write to file
	int response;
	sscanf(buffer,"HTTP/1.%*[01] %d ",&response);
	//printf("response : %d\n",response);
	if(response == 301){
		printf("error 301 - requested object moved, new location specified later in this msg\n");
		close(sockfd);
	}
	else if(response == 400){
		printf("error 400 - request msg not understood by server\n");
		close(sockfd);
	}
	else if(response == 404){
		printf("error 404 -requested document not found on this server\n");
		close(sockfd);
	}
	else if(response == 505){
		printf("error 505 -HTTP Version Not Supported");
		close(sockfd);
	}
	else if(response == 200){
		/*printf("from inside response =%d",response);
		printf("buffer = %s\n", buffer);*/
		printf("connecting to the server %s (%s) to retrieve the document %s\n",servername,ip_address,filename);
		printf("Ip Address = %s\n",ip_address);
		printf("receiving file\n");
		printf("saving to the file : %s\n",filename);
		printf("file name : %s\n",filename);

		FILE *fileptr=fopen(filename,"w+");
		//int buffer_length = strlen(buffer);
		
		/* you need to get the end of the HTTP headers (hint: use search string
		example in Slide 11*/
		char *end_of_HTTP_header = strstr(buffer, "\r\n\r\n");	
		//printf("HTTP Header end :%s\n",end_of_HTTP_header);


		int header_length = end_of_HTTP_header-buffer;
		//printf("Header-Length : %d\n",header_length);
		
		/* calculate the number of bytes left in the first message: "HTTPheader
		\r\n\r\ndata..data.."*/
		int data_length_after_HTTP_header = bytes_received-(header_length+4);
		//printf("data_length_after_HTTP_header :%d\n",data_length_after_HTTP_header);
		
		fwrite(end_of_HTTP_header+4, data_length_after_HTTP_header,1, fileptr);
		printf("First package : bytes_received =%d,header_length=%d,data_length_after_HTTP_header=%d\n",
				bytes_received,header_length,data_length_after_HTTP_header );
		
		int i=bytes_received;
		int j=1;

		/* loop to write the rest of the data to file"*/
		while((bytes_received=recv(sockfd,buffer, BUFFER_SIZE,0))!=0){
			fwrite(buffer, bytes_received,1, fileptr);	
			j++;
			i = i+bytes_received;
		}
		printf("times to receive : %d\n",j );
		printf("total bytes received : %d\n",i );

		fclose(fileptr);

	}else 
	//shutdown(sock,SHUT_RDWR);
	close(sockfd);
}
