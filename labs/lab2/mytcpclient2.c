/* include libraries */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>



int main (int argc, char *argv[])
{
   printf("Hello World\nPrudhvi Raj bhoga\nSecure Application development\n");
   //printf("Prudhvi Raj bhoga\n");
   //printf("Secure Application development\n")
   
	if(argc!=3){
	printf("usage: %s <servername> <port>\n",argv[0]);
	exit(0);
	}
      //printf("servername = %s, port=%s\n",argv[1],argv[2]);
	char *servername = argv[1];
	int port = atoi(argv[2]);
	printf("servername = %s, port=%d\n",servername, port);	
	int sockfd = socket(AF_INET, SOCK_STREAM,0); 
	if(sockfd<0){
		perror("cannot create socket");
		exit(1);
	}


	struct hostent *server_he;
	if((server_he = gethostbyname(servername)) == NULL){
	perror("cannot resolve the hostname");
	exit(2);
	}

	
	 struct sockaddr_in serveraddr;
	bzero((char *) &serveraddr,sizeof(serveraddr));
	serveraddr.sin_family=AF_INET;
	bcopy((char *) server_he->h_addr,
  	(char *) &serveraddr.sin_addr.s_addr,
     	server_he->h_length);
	serveraddr.sin_port=htons(port);

	
	int connected =connect(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
	if(connected<0){
	perror("cannot connect to the server");
	exit(3);
	}
	else
	printf("connect to the server %s at port %d\n",servername,port);
	
	char *msg="this is just a test message from client";
	int byte_sent;//=send(sockfd,msg,strlen(msg),0);
	char buffer[1024];
	//printf("enter your message:");
	bzero(buffer,1024);
	//sprintf(buffer, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", servername);
	
	// ---> code to get the content of url http://academic.udayton.edu/PhuPhung/sp2018secad-lab2.html
	//sprintf(buffer, "GET /PhuPhung/sp2018secad-lab2.html HTTP/1.0\r\nHost:academic.udayton.edu \r\n\r\n", servername);
	
	// ----> code to get content of the URL http://beej.us/guide/bgnet/pdf/bgnet_USLetter.pdf 
	sprintf(buffer,"GET /guide/bgnet/pdf/bgnet_USLetter.pdf file/1.0\r\nHost:beej.us \r\n\r\n", servername);
	//fgets(buffer,1024,stdin);
	byte_sent=send(sockfd,buffer,strlen(buffer),0);
	
	bzero(buffer,1024);
	int byte_received=recv(sockfd,buffer,1024,0);
	
	if(byte_received<0)
	{
	perror("error in reading");
	exit(4);
	}
	
	printf("received from server %s",buffer);

	// no coding below
	close(sockfd);
}
