<?php
  
  //echo "->auth.php";
  ini_set("session.cookie_httponly", True);
  ini_set("session.cookie_secure", True);
  ini_set("session.cookie_lifetime", 900);
//session_set_cookie_params(900, "/", "localhost", True, True); //this can also be used to set the cookie parameters 
//setcookie("test_cookie", "test", 900, '/', 'localhost', true, true);
  session_start();

  $old_sessionid = session_id();
  require 'mysql.php';
  if (isset($_POST["username"]) and isset($_POST["password"]) ){
    //echo "->auth.php:Debug>has username/password";
    if (mysql_checklogin_secure($_POST["username"],$_POST["password"])) {
      //generating a new session ID
      session_regenerate_id(); 
      $new_sessionid = session_id();
      //old and new session ID
      echo" Old Session id: $old_sessionid and New Session Id: $new_sessionid";
      $_SESSION["logged"] = TRUE;
      $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];   //to store browser info
      $_SESSION["username"] = $_POST["username"];      
  }
    else{
       echo "<script>alert('Invalid username/password');</script>";
       unset($_SESSION["logged"]); 
    }
  }
  if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
    //echo "->auth.php:Debug>You have not login. Please login first";
    header("Refresh:0; url=form.php");
    //header( 'Location: form.php' ) ;
    die();
  }

   if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){   //validating browser info

  echo "<script>alert('Session hijacking is detected!');</script>";
  header("Refresh:0; url=form.php");
  die();
  }
?>
<br>
<br>
