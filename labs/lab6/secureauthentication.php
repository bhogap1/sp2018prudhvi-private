<?php
  ini_set("session.cookie_httponly", True); //new
  ini_set("session.cookie_secure", True); //new 
  //set session lifetime = 15min
  //ini_set("session.cookie_lifetime", 900);
  //setting the cookie parameters
  session_set_cookie_params(900, "/", "localhost", True, True);
  session_start();
  $old_sessionId = session_id();
  //echo "->auth.php";
  require 'mysql.php';
  if (isset($_POST["username"]) and isset($_POST["password"]) ){
    //echo "->auth.php:Debug>has username/password";
    if (mysql_checklogin_secure($_POST["username"],$_POST["password"])) {
      //regenerating the session ID
      session_regenerate_id();
      $new_sessionId = session_id();
      echo "old session id = $old_sessionId; new session id = $new_sessionId";
      $_SESSION["logged"] = TRUE;
      
    $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
  }
    else{
	     echo "<script>alert('Invalid username/password');</script>";
	     unset($_SESSION["logged"]); 
    }
  }
  if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
    //echo "->auth.php:Debug>You have not login. Please login first";
    header("Refresh:0; url=form.php");
    //header( 'Location: form.php' ) ;
    die();
  }
  if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
  echo "<script>alert('Session hijacking is detected!');</script>";
  header("Refresh:0; url=form.php");
    //header( 'Location: form.php' ) ;
    die();
  }

?>
