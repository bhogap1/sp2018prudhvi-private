import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.lang.*;
import java.util.Map;

public class EchoServer {

    static ThreadList threadlist = new ThreadList();
    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);

        try {
            ServerSocket serverSocket = new ServerSocket(Integer.parseInt(args[0]));
            System.out.println("EchoServer is running at port " + Integer.parseInt(args[0]));
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("A client is connected ");
                EchoServerThread newthread = new EchoServerThread(threadlist, clientSocket);
                //threadlist.addThread(newthread);			
                newthread.start();
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port " +
                portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class EchoServerThread extends Thread {
    private Socket clientSocket = null;

    private ThreadList threadlist = null;

    private PrintWriter out = null;

    private ThreadList t = new ThreadList();
    private HashMap < String, String > userlist = t.getuserlist();

    public EchoServerThread(Socket socket) {
        clientSocket = socket;
    }
    public EchoServerThread(ThreadList threadlist, Socket socket) {
        this.threadlist = threadlist;
        clientSocket = socket;
    }

    /*HashMap<String,String> userlist = threadlist.getuserlist();
    HashMap<String,EchoServerThread> userthread = threadlist.getuserthread();*/
    public void run() {
        System.out.println("A new thread for user is running. number of connected users: " + threadlist.getNumberofThreads());
        try {
            /*PrintWriter*/
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;
            out.println("you have not logged in.To login, use <join>username:passwd");
            //checking the input <join>username:password
            while (!(inputLine = in .readLine()).contains("join") || !inputLine.contains(":")||inputLine.equals(null)) {
                if (inputLine.equals("<exit>")) {
                    //threadlist.sendToAll("To All: A client exists, the number of connected clients: " +(threadlist.getNumberofThreads()-1));
                    //threadlist.removeThread(this);
                    clientSocket.close();
                } else
                    send(" wrong format,use <join>username:passwd ");
            }
            // Checking the validity of credentials
            int connect1 = 0;
            int temp = 0;
            String[] arr = login(inputLine);
            String username = arr[0];
            String passwd = arr[1];
            //String[] arr = login(inputLine);
            while ((inputLine) != null && temp == 0) {
                username = arr[0];
                passwd = arr[1];
                if (threadlist.checkuser(username) == false) {
                    while ((inputLine) != null && connect1 == 0) {
                        username = arr[0];
                        passwd = arr[1];
                        for (Map.Entry < String, String > entry: userlist.entrySet()) {
                            if ((entry.getKey()).equals(username) && passwd.equals(entry.getValue())) {
                                out.println("succesfully connected ");
                                System.out.println("heloooooooooooooooo");
                                connect1 = 1;
                                //userthread.put(threadlist.currentThread(),username);
                                threadlist.addThread(this, username);
                                threadlist.sendToAll("To All: the number of connected users: " + (threadlist.getNumberofThreads()));
                                break;
                            }
                        }
                        if (connect1 == 0) {
                            out.println("wrong credentials ");
                            arr = login( in .readLine());
                        }
                    }
                    temp = 1;
                    break;
                } else if (temp == 0) {
                    out.println("user already logged in! ");
                    inputLine = in .readLine();
                    if (inputLine.equals("<exit>")) {
                        clientSocket.close();
                    }
                    arr = login(inputLine);
                }
            }
            //procedure to get <list>,<exit>,<chat>,<priv>
            int x = 0;
            while ((inputLine = in .readLine()) != null) {
                x = 0;
                String s = inputLine;
                if (s.contains("<list>") || s.contains("<exit>") || s.contains("<chat>") ||
                    s.contains("<priv>")) {
                    System.out.println("received from client: " + inputLine);
                    System.out.println("Echo back");
                    //getting the <list>
                    if (inputLine.equals("<list>")) {
                        ArrayList < String > list = threadlist.getlist();
                        for (int i = 0; i < list.size(); i++) {
                            send(list.get(i));

                        }
                        //implementing <exit>
                        x = 1;
                    } else if (inputLine.equals("<exit>")) {
                        threadlist.sendToAll("To All: A client exists, the number of connected clients: " + (threadlist.getNumberofThreads() - 1));
                        threadlist.removeThread(this);
                        clientSocket.close();
                        x = 1;

                    //condition for <priv>
                    } 
                    int y=0;
                    if (inputLine.contains("<priv>")) {
                    	if(inputLine.equals("<priv>")){
                    		send("wrong format: use format <priv>username:message");
                    		y=1;
                    	}
                    	else if (y==0){
                    		 String pname;
                        String message;
                        String[] m = inputLine.split(">");
                        String[] p = (m[1].toString()).split(":");
                        pname = p[0];
                        message = p[1];
	                        if (threadlist.sendPrivate(pname, message) == false) {
	                            send("user doesnot exist");
	                        }
                    	}
                       
                        x = 1;
                    } 
                    	int b =0;
                    if (threadlist != null && inputLine.contains("<chat>")) {
                    	if(inputLine.equals("<chat>")){
                    		send("input message");
                    		b=1;
                    	}
                        else if(b==0){
                        String[] i = inputLine.split(">");
                        String mesg;
                        mesg = i[1];
                        threadlist.sendToAll("To ALl <new messgae>:" + mesg);
                        }
                        x = 1;
                    }
                    //inputLine = in.readLine();
                }
                if (x == 0) {
                    send("Bad command . Please try either <list> | <chat>all:message | <priv>username:message");
                }

            }
        } catch (IOException ioe) {
            System.out.println("Exception in thread : " + ioe.getMessage());
        }
    }

    public void send(String message) {
        if (out != null)
            out.println(message);
    }
    //function to split the input into username and password
    public String[] login(String usr) {
        String username = null;
        String passwd = null;
        //threadlist.sendToAll("passwd " +inputLine);
        //send("passwd " +usr);
        String[] tokens = usr.split(">");
        if (!tokens[1].contains(":")) {
            username = tokens[1];
            passwd = "NULL";
        } else if (tokens[1].contains(":")) {
            String[] a = (tokens[1].toString()).split(":");
            username = a[0];
            passwd = a[1];
        }
        //send("username : "+username);
        //send("passwd : "+passwd);
        return new String[] {
            username,
            passwd
        };
    }
}

class ThreadList {
    //private ArrayList<EchoServerThread> threadlist = new ArrayList<EchoServerThread>();
    //Hashmap to store Username and Password
    HashMap < String, String > userlist = new HashMap < String, String > () {
        {
            //store the list of threads in this variable	
            put("prudhvi", "wU*~cQQ)A4R}*-");
            put("rahul", "G!F3k}e=VhtH9<(P");
            put("mani", "UwB4Fn]yf.8T95gm");
            put("sri", "zx9*UDT;K8Y/S@M");
        }
    };
    //Hashmap to store threadlist and username
    private static HashMap < EchoServerThread, String > threadlist = new HashMap < EchoServerThread, String > ();

    public ThreadList() {}

    public int getNumberofThreads() {
        //return the number of current threads
        return threadlist.size();
    }
    public synchronized void addThread(EchoServerThread newthread, String username) {
        //add the newthread object to the threadlist
        //inserting new thread and username into threadlist
        threadlist.put(newthread, username);

    }
    public synchronized void removeThread(EchoServerThread thread) {
        //remove the given thread from the threadlist
        threadlist.remove(thread);
    }
    public void sendToAll(String message) {
        //ask each thread in the threadlist to send the given message to its client
        /*Iterator<EchoServerThread> threadlistIterator =
        threadlist.iterator();
        while(threadlistIterator.hasNext()){
        	EchoServerThread thread =threadlistIterator.next();
        	thread.send(message);
        }*/
        //for loop to iterate through the hashmap and send message to all threads 
        for (EchoServerThread thread: threadlist.keySet()) {
            thread.send(message);
        }
    }

    //method to send message to the private user
    public boolean sendPrivate(String username, String message) {
        if (threadlist.containsValue(username)) {
            for (EchoServerThread thread: threadlist.keySet()) {
                if ((threadlist.get(thread)).equals(username))
                    thread.send("<priv>" + username + ":" + message);
            }
            return true;
        } else
            return false;
    }


    //To get keys from the value
    /*public static Object getKeyFromValue(threadlist hm, String value) {
	for (String o : hm.keySet()) {
	  if (hm.get(o).equals(value)) {
		return o;
	  }
	}
	return null;
}*/
	//method to send user list
    public HashMap < String, String > getuserlist() {
        return userlist;
    }

    //to get the list of connected users
    public ArrayList < String > getlist() {
        ArrayList < String > a = new ArrayList < String > ();
        for (String s: threadlist.values()) {
            a.add(s);
        }
        return a;
    }

    //to check if the user already logged in 
    public boolean checkuser(String username) {
        if (threadlist.containsValue(username)) {
            return true;
        } else
            return false;
    }


}